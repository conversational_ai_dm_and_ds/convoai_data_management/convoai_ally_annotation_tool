# Copyright 2018-2022 Streamlit Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import streamlit as st
from streamlit.logger import get_logger
import pandas as pd 
LOGGER = get_logger(__name__)

def run():
    st.set_page_config(
        page_title="Ally Annotation Tool",
        page_icon="👋",
        layout="wide"
    )
    st.image("img/ally-logo.png", use_column_width=False)

    st.write("# Welcome to Ally Annotation Tool! 👋")

    st.markdown(
        """
        Intent Optimization Tracking is developed to track intent performance over time by Conversational AI Data Science Group. 

        ### The tracking metrics include: 
        - Volume: The total number of converasations excluding transferred conversations
        - Containment Absolute: The total number of conversations that are contained.
        - Containment: The percengage of contained conversations vs volume.  
        - Falsely Classified As: Intent flag is Unknown with improper response.
        - Misidentified As: Intent flag is Known with improper response. 

        ### Grouping Parameters
        - Business Intent Group 
        - Aggregated by month of conversation happened 

        **👈 Select a Page from the sidebar** to see what the application can do!

    """
    
    )
    # st.image("img/bot.png", use_column_width=False)


if __name__ == "__main__":
    run()
