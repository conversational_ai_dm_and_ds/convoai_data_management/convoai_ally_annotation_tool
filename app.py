from multiprocessing.sharedctypes import Value
import streamlit as st
import os
import streamlit_authenticator as stauth  # pip install streamlit-authenticator
import yaml
from yaml import SafeLoader
import snowflake_con.utils as ut
from pathlib import Path

st.set_page_config(
        page_title="Ally Annotation Tool",
        page_icon="👋",
        layout="wide"
    )
#------------------------------------------------------------------

 #Add Ally logo to sidebar
st.sidebar.image('img/image005.png', use_column_width=True)
st.header("Ally Annotation Tool!")
#hashed_passwords = stauth.Hasher(passwords).generate()
file_path = Path(__file__).parent / "config.yaml"

with file_path.open('rb') as file:
    config = yaml.load(file, Loader=SafeLoader)


authenticator = stauth.Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)

#----------------------------------------

#----------------------------------------

name, authentication_status, username = authenticator.login('Login', 'main')
#name, authentication_status = authenticator.login('Login','main')
#-------------------------------------End of Authenticator #________________________________________

root = os.path.join(os.path.dirname(__file__))
#st.write(root)
if st.session_state['authentication_status']:
    st.write('Welcome *%s*' % (st.session_state['name']))
    #st.title(f'''Let's Annotate''') 
elif st.session_state['authentication_status'] == False:
    st.error('Username/password is incorrect')
elif st.session_state['authentication_status'] == None:
    st.warning('Please enter your username and password')
    
if authentication_status:
  dashboards = {
      "Annotation Overview": os.path.join(root, "pages_menu/annotation_overview.py"),
      "Ally Data Annotation ": os.path.join(root, "pages_menu/ally_data_annotation.py"),
      "Data Annotation - Bulk Labeling ": os.path.join(root, "pages_menu/annotation_bulk_labeling.py"),
      "Data Annotation - Data Quality ": os.path.join(root, "pages_menu/annotation_data_quality.py"),
      "Data Annotation - Holdout Data Labeling ": os.path.join(root, "pages_menu/annotation_data_holdout.py"),
      "Data Annotation - Reporting ": os.path.join(root, "pages_menu/annotation_reporting.py"),
     
  }
  

  choice_from_url = query_params = st.experimental_get_query_params().get("example", ["Annotation Overview"])[0]
  index = list(dashboards.keys()).index(choice_from_url)
  
  choice = st.sidebar.radio("Services", list(dashboards.keys()), index=index)
  
  path = dashboards[choice]
  
  with open(path, encoding="utf-8") as code:
      c = code.read()
      exec(c, globals())
  
  authenticator.logout("Logout", "sidebar")
      #with st.expander('Code for this example:'):
          #st.markdown(f"""``` python {c}```""")
