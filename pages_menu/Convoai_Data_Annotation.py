import pickle
from pathlib import Path
import streamlit as st 
import snowflake_con.db_connection as db_connection
import snowflake_con.query_collection as qc 
import time 
import pandas as pd 
from st_aggrid import AgGrid, GridUpdateMode
from st_aggrid.grid_options_builder import GridOptionsBuilder
import streamlit_authenticator as stauth  # pip install streamlit-authenticator
import yaml
from yaml import SafeLoader
import snowflake_con.utils as ut

# emojis: https://www.webfx.com/tools/emoji-cheat-sheet/
st.markdown("# Data Annotation Check ❄️")
st.sidebar.markdown("# Data Annotation ❄️")


# --- USER AUTHENTICATION ---
userid="PZLYH9"
account='ally.us-east-1.privatelink'
warehouse='WH_TEAM_TECH_CONVOAI_ME' 
database='TEAM_TECH_CONVOAI_P'
schema='CORE'
# running environment: local, prod, workbench 
environment='prod'
id_rsa_filename="id_rsa" 
passcode_filename='id_rsa_passcode'
xqc = qc.select_annotation_user()

def build_bot_chat(response_string):
  return """<li class=\"bot\" style=\"display:inline-block;clear:both;padding:20px;border-radius:30px;margin-bottom:2px;font-family:Helvetica, Arial, sans-serif;background:#eee;float:left;border-bottom-left-radius:5px;max-width:400px\">""" + response_string + """</li>"""

def build_user_chat(user_string):
  return """<li class=\"user\" style=\"display:inline-block;clear:both;padding:20px;border-radius:30px;margin-bottom:2px;font-family:Helvetica, Arial, sans-serif;background:#0084ff;float:right;color:#fff;border-bottom-right-radius:5px;max-width:400px\">""" + user_string + """</li>"""

def get_table_data(query): 
    t0 = time.time()
    sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
    sc.create_cursor() 
    
    df = sc.update_table(query)

    print(f"INFO: Updating Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
    sc.cursor.close()
    return df

df = get_table_data(xqc)

#------------------------------------------------------------------

hashed_passwords = stauth.Hasher(['123', '456', 'password']).generate()
print(hashed_passwords)
#hashed_passwords = stauth.Hasher(passwords).generate()
file_path = Path(__file__).parent / "config.yaml"

with file_path.open('rb') as file:
    config = yaml.load(file, Loader=SafeLoader)


authenticator = stauth.Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)

#----------------------------------------

#----------------------------------------

name, authentication_status, username = authenticator.login('Login', 'main')
#name, authentication_status = authenticator.login('Login','main')

if st.session_state['authentication_status']:
    st.write('Welcome *%s*' % (st.session_state['name']))
    st.title('Some content')
elif st.session_state['authentication_status'] == False:
    st.error('Username/password is incorrect')
elif st.session_state['authentication_status'] == None:
    st.warning('Please enter your username and password')
    
if authentication_status:
  # ---- SIDEBAR ----
  authenticator.logout("Logout", "sidebar")
    
  # annotated data date range
  start_date = '2022-09-01'
  end_date = '2022-09-02'
  # snowflake NLU files Config - revise to your settings 
  
  
  @st.experimental_memo(show_spinner=True, suppress_st_warning=True)
  def query_data(start_date, end_date): 
      t0 = time.time()
      sc = db_connection.snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      sc.create_cursor() 
      # pulling all conversations including annotated and not annotated.
      query = f'''
                SELECT distinct
                  CONVERSATION_TURN_KEY  as CONVERSATION_TURN_KEY
                  ,CONVERSATION_ID  
                  ,TURN_INDEX as TURN_INDEX
                  ,cast(a.LOAD_DATE as date) as LOAD_DATE
                  ,TIMESTAMP_OF_USER_TEXT as TIMESTAMP_OF_USER_TEXT
                  , USER_UTTERANCE
                  ,(BOT_RESPONSE ) as BOT_RESPONSE
                  ,b.intent_business_groups as BUSINESS_INTENT_NAME
                  ,a.name as BOT_INTENT_NAME
                  ,CONFIDENCE
                  ,TRANSFERRED_TO_LIVE_AGENT as TRANSFER_TO_AGENT
                  ,REASON_FOR_TRANSFER as TRANSFER_REASON
                  ,CHANNEL as CHANNEL
                  ,(est_conversation_level) as CONVERSATION_LEVEL
                  ,replace(est_if_abandoned, 'nan', '') as ABANDONED_REASONS
                  ,case
                      when (transferred_to_live_agent = TRUE)  
                      and ( substr(lower(USER_UTTERANCE), 1, 5) = '/oos_') then 'designed_transfer'
                      when (final_est_intent_flag = 'Est Known Intent') then 'known intent'
                      when (final_est_intent_flag = 'Est Unknown Intent') then 'unknown intent'
                      when (final_est_intent_flag = 'Escalation') then 'escalation'
                      else replace(est_if_transferred, 'nan', '')
                      end as TRANSFER_REASONS
                  ,replace(est_if_contained, 'nan', '') as CONTAINED_REASONS
                  ,replace(EST_ESCALATION_REASON, 'nan', '') as ESCALATION_REASONS
                  ,EST_RESPONSE_ACCURACY as RESPONSE_GROUP  
                  ,'' as Annotator
                  ,'' as Reviewed_By
                  ,'' as reviewer_agree_on_first
                  ,FINAL_EST_INTENTS
                  ,FINAL_EST_INTENT_FLAG
                  ,EST_RESPONSE_ACCURACY
                  ,replace(est_conversation_level_flag, 'nan', '') as EST_CONVERSATION_LEVEL_FLAG    
                  , replace(est_conversation_level, 'nan', '') as EST_CONVERSATION_LEVEL
                  , replace(est_if_abandoned, 'nan', '') as EST_ABANDON_REASON    
                  ,replace(est_if_transferred, 'nan', '') as EST_TRANSFER_REASON    
                  ,replace(est_if_contained, 'nan', '') as Est_Contained_Reason    
                  ,replace(EST_ESCALATION_REASON, 'nan', '') as EST_ESCALATION_REASON    
                  , greeting_closing_intents as greeting_closing_intents    
                  , '' as  User_Type
                  ,'N' as Reviewed
                  ,'' as EST_vs_Actual
                  ,'' as Is_valid_intent
          from TEAM_TECH_CONVOAI_P.CORE.TBL_BASE_CHAT_LOG_D a
          LEFT OUTER JOIN  TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_BUSINESS_INTENT_NAMES b
          ON (a.name = b.intent)
          Where  
           ( CAST(a.LOAD_DATE as DATE) >= CAST('{start_date}' as DATE))
            and ( CAST(a.LOAD_DATE as DATE) <= CAST('{end_date}' as DATE)) 
          '''
      
      df = sc.query_data(query)
      df = df.sort_values(['CONVERSATION_ID', 'TIMESTAMP_OF_USER_TEXT'])
      df['TIMESTAMP_OF_USER_TEXT'] = pd.to_datetime(df['TIMESTAMP_OF_USER_TEXT'])
      df['month_year'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%B-%Y')) 
      df['INTENT_NAME'] = df['BOT_INTENT_NAME'].str.lower()
      df['INTENT_NAME'] = df['BOT_INTENT_NAME'].str.strip()
      df['month_year'] = pd.Categorical(df['month_year'], ["February-2022", "March-2022", "April-2022", "May-2022", "June-2022","July-2022","August-2022","September-2022"
      ])
      df["CONVERSATION_LEVEL"] = df[['TRANSFER_TO_AGENT', 'CONVERSATION_LEVEL']].apply(lambda x: ut.check_trnfer_agent(x[0] , x[1] ), axis=1 )
      df["ABANDON_REASONS"] = df[['TRANSFER_TO_AGENT', 'ABANDONED_REASONS']].apply(lambda x: ut.check_trnfer_agent_abandon(x[0] , x[1] ), axis=1 )
      df["CONVERSATION_LEVEL"] = df[['CONVERSATION_LEVEL']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )
      df["USER_UTTERANCE"] = df[['USER_UTTERANCE']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )
      df["BOT_RESPONSE"] = df[['BOT_RESPONSE']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )

      sc.cursor.close()
      return df
  def update_table_data(query): 
      t0 = time.time()
      sc = db_connection.snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      sc.create_cursor() 
      df = sc.update_table(query)
  
      print(f"INFO: Updating Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
      sc.cursor.close()
      return df
  def build_update_str(TBL_Name, INTENT_BUSINESS_NAME, INDEX, INTENT_NAME, CONVERSATION_LEVEL,ABANDONED_REASONS
  ,TRANSFER_REASONS, CONTAINED_REASONS, 
  ESCALATION_REASON, ANNOTATOR, REVIEWED_BY, 
  CONVERSATION_TURN_KEY, REVIEWER_AGREE_ON_FIRST, RESPONSE_GROUP):
     
    qstring = f'''update {TBL_Name} 
            set INTENT_BUSINESS_NAME = '{INTENT_BUSINESS_NAME}'
            ,INTENT_NAME = '{INTENT_NAME}'
            ,CONVERSATION_LEVEL = '{CONVERSATION_LEVEL}'
            ,ABANDONED_REASONS = '{ABANDONED_REASONS}'
            , TRANSFER_REASONS = '{TRANSFER_REASONS}'
            ,CONTAINED_REASONS = '{CONTAINED_REASONS}'
            ,ESCALATION_REASON = '{ESCALATION_REASON}'
            ,ANNOTATOR = '{ANNOTATOR}'
            ,REVIEWED_BY = '{REVIEWED_BY}'
            ,RESPONSE_GROUP = '{RESPONSE_GROUP}'
            ,REVIEWER_AGREE_ON_FIRST = '{REVIEWER_AGREE_ON_FIRST}'
            where CONVERSATION_TURN_KEY = '{CONVERSATION_TURN_KEY}';'''
    return qstring
  
  # get the chat data
  all_data = query_data(start_date, end_date)
  
 
  
  # display the conversations in September, investigating the conversation level is unique for one conversation. 
  with st.sidebar: #1st filter                                     
    FirstFilter = st.multiselect(label="Select Est Conversation Level", 
    options=list(all_data["EST_CONVERSATION_LEVEL_FLAG"].unique()),
    default=["Contained"] )   
    
  df = all_data.query("EST_CONVERSATION_LEVEL_FLAG == @FirstFilter")

  with st.form("my_form"):
    gd = GridOptionsBuilder.from_dataframe(df)
    gd.configure_pagination(enabled=True)
    gd.configure_default_column(editable=True, groupable=True)
    sel_mode = st.radio('Selection Type', options=['single', 'multiple'])

#Select column select values
    gd.configure_column('CONVERSATION_LEVEL',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['Abandoned',
                      'Contained',
                      'Transferred',
                      'Known data Issue',
                      '']},
      cellEditorPopup=True
    )
    gd.configure_column('ABANDONED_REASONS',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['Greeting',
                      'Intent',
                      '']},
      cellEditorPopup=True
    )
    gd.configure_column('ESCALATION_REASON',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['Greeting',
                      'Intent',
                      '']},
      cellEditorPopup=True
    )
    gd.configure_column('TRANSFER_REASONS', 
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['known intent',
                      'unknown intent',
                      'escalation',
                      'sentiment',
                       'designed_transfer',
                      '']},
      cellEditorPopup=True
    )
    gd.configure_column('CONTAINED_REASONS',  
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['Full Containment',
                      'Partial Containment',
                       '']},
      cellEditorPopup=True
    )
    gd.configure_column('RESPONSE_GROUP',  
      cellEditor='agRichSelectCellEditor', 
      cellEditorParams={'values':['improper response',
                      'proper response',
                       '']},
      cellEditorPopup=True
    )
    gd.configure_column('ANNOTATOR',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['PZLYH9',
                      'TZYTT7',
                      'KZ60Z3',
                      'TZX8QT',
                      'PZSW95',
                      'QZ6JS1',
                      'RZ383S',
                      'xz47sx']},
      cellEditorPopup=True
    )
    gd.configure_column('REVIEWED_BY',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['PZLYH9',
                      'TZYTT7',
                      'KZ60Z3',
                      'TZX8QT',
                      'PZSW95',
                      'QZ6JS1',
                      'RZ383S',
                      'xz47sx']},
      cellEditorPopup=True
    )
    gd.configure_column('REVIEWER_AGREE_ON_FIRST',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':['Y','N']},
      cellEditorPopup=True
    )
    gd.configure_grid_options(enableRangeSelection=True)
    gd.configure_selection(selection_mode=sel_mode, use_checkbox=True)
    gridoptions = gd.build()
    grid_table = AgGrid(df, gridOptions=gridoptions,
                        update_mode=GridUpdateMode.SELECTION_CHANGED | GridUpdateMode.VALUE_CHANGED,
                        #
                        height=500,
                        allow_unsafe_jscode=True,
                        # enable_enterprise_modules = True,
                        theme='streamlit'
                        )
    submitted = st.form_submit_button("Submit")
    if submitted:
      sel_row = grid_table["selected_rows"]
      st.subheader("Output")
      st.write(sel_row)
      
      
       
      try:
        # TODO: write code...
        df_selected = pd.DataFrame(sel_row)
        html_chat_string = """<ul style=\"list-style:none;margin:0;padding:0;width:500px;\">"""
        for i, r in df_selected.iterrows():
          html_chat_string += build_user_chat(r['USER_UTTERANCE'])
          html_chat_string += build_bot_chat(r['BOT_RESPONSE'])
          #html_chat_string += build_user_chat("Help with buckets")
        html_chat_string += """</ul>"""
      except IndexError:
        df_selected = pd.DataFrame()
        st.write(df_selected.empty)
try:      
  if not df_selected.empty:
    st.markdown(html_chat_string,unsafe_allow_html=True)
    if st.button('Update db', key=1):
      for i, r in df_selected.iterrows():
        ustring = build_update_str('TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG', i, 
        r['BUSINESS_INTENT_NAME'],
        r['BOT_INTENT_NAME'], 
        r['CONVERSATION_LEVEL'],
        r['ABANDONED_REASONS'],
        r['TRANSFER_REASONS'], r['CONTAINED_REASONS'], 
        r['ESCALATION_REASONS'], 
        r['ANNOTATOR'], r['REVIEWED_BY'], 
        r['CONVERSATION_TURN_KEY'], r['REVIEWER_AGREE_ON_FIRST'], r['RESPONSE_GROUP'])
        st.write(ustring)
        if not df_selected.empty: 
          uD = update_table_data(ustring)
    
        st.write('##### Updated db')
        #df_update = pd.read_sql('SELECT * FROM product', con=conn)
        #st.write(uD.count())
except NameError: 
    st.markdown("Select a conversation and **Click** submit to display the chat bubble ❄️")
    

