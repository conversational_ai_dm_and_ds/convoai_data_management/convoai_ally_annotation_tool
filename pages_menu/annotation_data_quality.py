from distutils import errors
from distutils.log import error
import streamlit as st
import pandas as pd 
import numpy as np
import altair as alt
from itertools import cycle

from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode, JsCode
import streamlit as st 
import snowflake_con.db_connection as db_connection 
from st_aggrid import AgGrid, GridUpdateMode
from st_aggrid.grid_options_builder import GridOptionsBuilder
import time 
import pandas as pd 

np.random.seed(42)
#st.markdown("# Data Quality Check ❄️")
st.markdown("Data Quality Check")
st.sidebar.markdown("# Data Quality Check ❄️")


# annotated data date range
st.header("")
col1, col2 = st.columns(2)
with col1:
  a_start_date = st.date_input("Start Date", value=pd.to_datetime("today", format="%Y-%m-%d"), key="sdate1")
with col2:
  a_end_date = st.date_input("End Date", value=pd.to_datetime("today", format="%Y-%m-%d"), key="edate1")

#create empty dataframes.
"""annotated_total = pd.DataFrame(columns = [ 'CONVERSATION_TURN_KEY'
      #,'CONVERSATION_ID'  -- ValueError: cannot insert CONVERSATION_ID, already exists
      ,'TURN_INDEX'
      ,'LOAD_DATE'
      ,'TIMESTAMP_OF_USER_TEXT'
      ,'USER_UTTERANCE'
      ,'BOT_RESPONSE'
      ,'INTENT_BUSINESS_NAME'
      ,'INTENT_NAME'
      ,'TRANSFERRED_TO_LIVE_AGENT'
      ,'REASON_FOR_TRANSFER'
      ,'CHANNEL'
      ,'CONVERSATION_LEVEL'
      ,'ABANDONED_REASONS'
      ,'TRANSFER_REASONS'
      ,'CONTAINED_REASONS'
      ,'ESCALATION_REASONS'
      ,'RESPONSE_GROUP'  
      ,'ANNOTATOR'
      ,'REVIEWED_BY'
      ,'EST_RESPONSE_ACCURACY'
      ,'REVIEWER_AGREE_ON_FIRST'
      ,'ANNOTATED_RESPONSE_GROUP'
      ,'EXPECTED_ANNOTATION'
      ,'RISK_ERROR_TYPE_I'
      ,'RISK_ERROR_TYPE_II'
      ,'RISK_ERROR_TYPE_III'
      ,'MONTH_YEAR'])
"""
#start_date = '2022-05-01'
#end_date = '2022-09-30'
# snowflake NLU files Config - revise to your settings 
userid="PZLYH9"
account='ally.us-east-1.privatelink'
warehouse='WH_TEAM_TECH_CONVOAI_ME' 
database='TEAM_TECH_CONVOAI_P'
schema='CORE'
# running environment: local, prod, workbench 
environment='prod'
id_rsa_filename="id_rsa" 
passcode_filename='id_rsa_passcode'

@st.experimental_memo(show_spinner=True, suppress_st_warning=True)

def query_data(start_date, end_date): 
    t0 = time.time()
    sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
    sc.create_cursor() 
    # pulling all conversations including annotated and not annotated.
    query = f'''
        select B.*, A.risk_error_type_i, A.risk_error_type_ii, A.risk_error_type_iii
        from TEAM_TECH_CONVOAI_P.CORE.TBL_FACT_CHAT_COVERAGE A,
        TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG B
        where A.CONVERSATION_TURN_KEY = B.CONVERSATION_TURN_KEY
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) >= CAST('{start_date}' as DATE))
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) <= CAST('{end_date}' as DATE))
        order by B.CONVERSATION_ID,B.TIMESTAMP_OF_USER_TEXT  
        '''
    ## and B.EXPECTED_ANNOTATION = TRUE
    #st.write(query)
    df = sc.query_data(query)

    # annotated_df = annotated_df[annotated_df.ANNOTATOR!='']
    df = df.sort_values(['CONVERSATION_ID', 'TIMESTAMP_OF_USER_TEXT'])
    df['TIMESTAMP_OF_USER_TEXT'] = pd.to_datetime(df['TIMESTAMP_OF_USER_TEXT'])
    df['MONTH_YEAR'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%B-%Y')) 
    df['INTENT_NAME'] = df['INTENT_NAME'].str.lower()
    df['INTENT_NAME'] = df['INTENT_NAME'].str.strip()
    df['MONTH_YEAR'] = pd.Categorical(df['MONTH_YEAR'], ["February-2022", "March-2022", "April-2022", "May-2022", "June-2022","July-2022","August-2022","September-2022"
    ])
    print(f"INFO: Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
    sc.cursor.close()
    df.columns = df.columns.str.upper()
    return df
@st.cache(allow_output_mutation=True)
def build_update_str(TBL_Name, 
      INDEX,
      INTENT_BUSINESS_NAME, 
      INTENT_NAME, 
      CONVERSATION_LEVEL,
      ABANDONED_REASONS,
      TRANSFER_REASONS, 
      CONTAINED_REASONS, 
      ESCALATION_REASON, 
      ANNOTATOR, 
      REVIEWED_BY, 
      RESPONSE_GROUP,
      REVIEWER_AGREE_ON_FIRST,
      CONVERSATION_TURN_KEY):
  qstring = f'''update {TBL_Name} 
            set INTENT_BUSINESS_NAME = '{INTENT_BUSINESS_NAME}'
            ,INTENT_NAME = '{INTENT_NAME}'
            ,CONVERSATION_LEVEL = '{CONVERSATION_LEVEL}'
            ,ABANDONED_REASONS = '{ABANDONED_REASONS}'
            , TRANSFER_REASONS = '{TRANSFER_REASONS}'
            ,CONTAINED_REASONS = '{CONTAINED_REASONS}'
            ,ESCALATION_REASON = '{ESCALATION_REASON}'
            ,ANNOTATOR = '{ANNOTATOR}'
            ,REVIEWED_BY = '{REVIEWED_BY}'
            ,ANNOTATED_RESPONSE_GROUP = '{RESPONSE_GROUP}'
            ,REVIEWER_AGREE_ON_FIRST = '{REVIEWER_AGREE_ON_FIRST}'
            where CONVERSATION_TURN_KEY = '{CONVERSATION_TURN_KEY}';'''
  return qstring

def update_table_data(query): 
  t0 = time.time()
  sc = db_connection.snowflake_connector(userid=userid, 
                          account=account,
                          warehouse=warehouse, 
                          database=database, 
                          schema=schema)
  #sc.create_cursor(environment=environment, id_rsa_filename=id_rsa_filename, passcode_filename=passcode_filename, userid=userid) 
  sc.create_cursor()
  df = sc.update_table(query)

  print(f"INFO: Updating Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
  sc.cursor.close()
  return df
# get the chat data
all_data = query_data(a_start_date, a_end_date)

# get the annotated 
#st.dataframe(all_data)
annotated_df = all_data[all_data.EXPECTED_ANNOTATION==True]
# get the transferred conversation ids 
convo_id_transferred = all_data[all_data.TRANSFERRED_TO_LIVE_AGENT==True].CONVERSATION_ID.unique()
# get the non-annotated and non-transferred conversations 
no_annotate_df = all_data[(all_data.EXPECTED_ANNOTATION==False)&(~all_data.CONVERSATION_ID.isin(convo_id_transferred))]
# group by month_year, get the conversation id counts 

if all_data.size != 0:
  total = all_data.groupby('MONTH_YEAR')['CONVERSATION_ID'].nunique().reset_index()
  total.columns = ['MONTH_YEAR', 'total conversation count']

  annotated_total =pd.DataFrame()
  if annotated_df.size != 0:
    annotated_total = annotated_df.groupby('MONTH_YEAR')['CONVERSATION_ID'].nunique().reset_index()
    annotated_total.columns = ['MONTH_YEAR', 'annotated conversation count']

  transferred = all_data[all_data.TRANSFERRED_TO_LIVE_AGENT==True].groupby('MONTH_YEAR')['CONVERSATION_ID'].nunique().reset_index()
  transferred.columns = ['MONTH_YEAR', 'transferred conversation count']

  if no_annotate_df.size != 0:
    no_transferred_no_annotated = no_annotate_df.groupby('MONTH_YEAR')['CONVERSATION_ID'].nunique().reset_index()
    no_transferred_no_annotated.columns = ['MONTH_YEAR', 'non-transferred and non-annotated conversation count']
  
    # merge those tables 
    #st.dataframe(annotated_total)
  if annotated_total.size != 0:  
    df1 = pd.merge(total, annotated_total, on='MONTH_YEAR')
    df2 = pd.merge(df1, transferred, on='MONTH_YEAR')
    df3 = pd.merge(df2, no_transferred_no_annotated, on='MONTH_YEAR')

# display the table with all statistics 
    st.table(df3[df3['total conversation count']!=0])

# display the conversations in September, investigating the conversation level is unique for one conversation. 
    annotated_df_sep = annotated_df[annotated_df.MONTH_YEAR == 'September-2022']
    annotated_df_sep.drop('CONVERSATION_ID',axis=1) #ValueError: cannot insert CONVERSATION_ID, already exists
    annotated_df_sep_gr = annotated_df_sep.groupby('CONVERSATION_ID').apply(lambda x: ''.join(x.CONVERSATION_LEVEL)).reset_index()
    #st.dataframe(annotated_df_sep_gr)
    annotated_df_sep_gr.columns = ['CONVERSATION_ID', 'LEVEL']
  
    conv_level = annotated_df_sep_gr.LEVEL.unique()
    st.markdown('Note: Conversation Level is aggregated by conversation id, investigating what conversation flag are there in one conversation. \n  Ideally one conversation has one conversation level flag.')
    conv_selected = st.selectbox("Converation Level:", conv_level) 
    conv_id_selected = st.selectbox("Converation IDs:", annotated_df_sep_gr[annotated_df_sep_gr.LEVEL==conv_selected].CONVERSATION_ID.unique())
    st.markdown(f"Total number of conversations: {len(conv_id_selected)} in the selected conversation levels")
    #st.dataframe(annotated_df_sep[annotated_df_sep.CONVERSATION_ID == conv_id_selected])
    df = annotated_df_sep[annotated_df_sep.CONVERSATION_ID == conv_id_selected]
    gd = GridOptionsBuilder.from_dataframe(df)
    gd.configure_pagination(enabled=True)
    gd.configure_default_column(editable=True, groupable=True)
    cellsytle_jscode = JsCode("""
    function(params) {
        if (params.value == 'A') {
            return {
                'color': 'white',
                'backgroundColor': 'darkred'
            }
        } else {
            return {
                'color': 'black',
                'backgroundColor': 'white'
            }
        }
    };
    """)
    gd.configure_column("RESPONSE_GROUP", cellStyle=cellsytle_jscode)
    
#Select column select values
    gd.configure_column('CONVERSATION_LEVEL',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'Abandoned',
                        'Contained',
                        'Transferred',
                        'Known data Issue'
                        '']},
        cellEditorPopup=True
      )
    gd.configure_column('ABANDONED_REASONS',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'Greeting',
                        'Intent']},
        cellEditorPopup=True
      )
    gd.configure_column('ESCALATION_REASON',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'Greeting',
                        'Intent']},
        cellEditorPopup=True
      )
    gd.configure_column('TRANSFER_REASONS', 
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'known intent',
                        'unknown intent',
                        'escalation',
                        'sentiment',
                         'designed_transfer']},
        cellEditorPopup=True
      )
    gd.configure_column('CONTAINED_REASONS',  
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'Full Containment',
                        'Partial Containment']},
        cellEditorPopup=True
      )
    gd.configure_column('ANNOTATED_RESPONSE_GROUP',  
        cellEditor='agRichSelectCellEditor', 
        cellEditorParams={'values':['improper response',
                        'proper response']},
        cellEditorPopup=True
      )
    gd.configure_column('ANNOTATOR',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'PZLYH9',
                        'TZYTT7',
                        'KZ60Z3',
                        'TZX8QT',
                        'PZSW95',
                        'QZ6JS1',
                        'RZ383S',
                        'xz47sx']},
        cellEditorPopup=True
      )
    gd.configure_column('REVIEWED_BY',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':[
                        '',
                        'PZLYH9',
                        'TZYTT7',
                        'KZ60Z3',
                        'TZX8QT',
                        'PZSW95',
                        'QZ6JS1',
                        'RZ383S',
                        'xz47sx']},
        cellEditorPopup=True
      )
    gd.configure_column('REVIEWER_AGREE_ON_FIRST',
        cellEditor='agRichSelectCellEditor',
        cellEditorParams={'values':['','Y','N']},
        cellEditorPopup=True
      )
    sel_mode = st.radio('Selection Type', options=['single', 'multiple'])
    gd.configure_selection(selection_mode=sel_mode, use_checkbox=True)
    gridoptions = gd.build()
    grid_table = AgGrid(df, gridOptions=gridoptions,
                      update_mode=GridUpdateMode.SELECTION_CHANGED | GridUpdateMode.VALUE_CHANGED,
                      #
                      height=500,
                      allow_unsafe_jscode=True,
                      # enable_enterprise_modules = True,
                      theme='streamlit'
                      )
                      
    sel_row = grid_table["selected_rows"]
    st.subheader("Output")
    st.write(sel_row)
    try:
      # TODO: write code...
      df_selected = pd.DataFrame(sel_row)
    except IndexError:
      df_selected = pd.DataFrame()
      st.write(df_selected.empty)
      
    if not df_selected.empty:
      #st.markdown(html_chat_string,unsafe_allow_html=True)
      if st.button('Update db', key=1):
        for i, r in df_selected.iterrows():
          ustring = build_update_str('TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG', i, 
          r['INTENT_BUSINESS_NAME'],
          r['INTENT_NAME'], 
          r['CONVERSATION_LEVEL'],
          r['ABANDONED_REASONS'],
          r['TRANSFER_REASONS'], 
          r['CONTAINED_REASONS'], 
          r['ESCALATION_REASON'], 
          r['ANNOTATOR'], 
          r['REVIEWED_BY'], 
          r['ANNOTATED_RESPONSE_GROUP'],
          r['REVIEWER_AGREE_ON_FIRST'],
          r['CONVERSATION_TURN_KEY'] )
          st.write(ustring)
          if not df_selected.empty: 
            uD = update_table_data(ustring)
    
    st.markdown("### Interesting checking out conversations that are not transferred and not annotated??") 
    samples = no_annotate_df
    conv_id_selected2 = st.selectbox("Conversation IDs:", samples.CONVERSATION_ID.unique())
    st.dataframe(samples[samples.CONVERSATION_ID == conv_id_selected2])

