from distutils import errors
from distutils.log import error
import streamlit as st
import pandas as pd 
import numpy as np
import altair as alt
from itertools import cycle

from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode, JsCode


def run():

    st.image("img/artificial-intelligence.jpg", use_column_width=False)

    st.write("# Welcome to Ally Annotation Tool! 👋")

    st.markdown(
        """
        Welcome to Annotation Nation!
        Data annotation is a process where a human data annotator goes into a raw data set and adds categories, 
        labels, and other contextual elements, so machines can read and act upon the information. The process of labeling 
        data is called "Ground Truth". ‘Ground truth’ represents the objective, humanly verifiable observation of the state of 
        an object or an information that might be considered a fact. In that context we speak of ‘ground truth data annotations’ 
        or ‘ground truth labels’ - humanly provided classifications of the data on which the algorithms are trained or against which they are evaluated.​

        ### These questions guides us to Ground Truths: 
        - Is our Chat Bot trained to handle the user requested task(s) **(known/Unknown)**
        - Did the bot respond correctly **(Proper/Improper Response)**
        - Did the bot complete the entire task – **(Contained)**
        - Did the user leave the session – **(Abandon)**
        - Did the bot transfer properly – **(Transfer)**
        - Is the bot handling user escalations properly - **(Transfer/escalation)**
        - Is the bot classifying Intent correctly – **(Intent Name)**


        **👈 Select a Page from the sidebar** to see what the application can do!

    """
    
    )
    # st.image("img/bot.png", use_column_width=False)

if __name__ == "__main__":
    run()

