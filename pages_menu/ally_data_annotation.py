from distutils import errors
from distutils.log import error
import streamlit as st
import pandas as pd 
import numpy as np
import altair as alt
from itertools import cycle
import pickle
import os

from pathlib import Path
import streamlit as st 
import snowflake_con.db_connection as db_connection
import snowflake_con.query_collection as qc 
import time
import datetime as dt 
from datetime import datetime
from datetime import date , timedelta

import pandas as pd 
from st_aggrid import AgGrid, GridUpdateMode
from st_aggrid.grid_options_builder import GridOptionsBuilder
import streamlit_authenticator as stauth  # pip install streamlit-authenticator
import yaml
from yaml import SafeLoader
import snowflake_con.utils as ut

from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode, JsCode
import qa_checks.QualityAssurance as QA

license_key = "For_Trialing_ag-Grid_Only-Not_For_Real_Development_Or_Production_Projects-Valid_Until-2_February_2023_[v2]_MTY3NTI5NjAwMDAwMA==5019cf4ce03850168f15b7cdbd2c2378"
def format_float(value):
    return f'{value:,.2f}'
  
pd.options.display.float_format = format_float
np.set_printoptions(precision=2)
np.random.seed(42)
#-------------------------------------
#The grid will redraw itself and reload the data whenever the key of the component changes.
#If key=None or not set at all, streamlit will compute a hash from AgGrid() parameters to use as a unique key.
#This can be simulated by changing the grid height, for instance, with the slider:

key="'an_unique_key'" #
reload_data=False

# --- USER AUTHENTICATION ---
userid=os.getenv('ZID')
print(userid)
account='ally.us-east-1.privatelink'
warehouse='WH_TEAM_TECH_CONVOAI_ME' 
database='TEAM_TECH_CONVOAI_P'
schema='CORE'
# running environment: local, prod, workbench 
environment='prod'
id_rsa_filename="id_rsa" 
passcode_filename='id_rsa_passcode'
xqc = qc.select_annotation_user()

#Set all empty dataframes here
df = pd.DataFrame(columns=['CONVERSATION_TURN_KEY','CONVERSATION_ID', ])
df_week_date_start = pd.DataFrame(columns=['DATE_KEY'])
df_week_date_end = pd.DataFrame(columns=['DATE_KEY'])
all_data = pd.DataFrame(columns = [ 'CONVERSATION_TURN_KEY'
      ,'CONVERSATION_ID'  
      ,'TURN_INDEX'
      ,'USER_UTTER_DATE'
      ,'USER_UTTERANCE'
      ,'BOT_RESPONSE'
      ,'BOT_RESPONSE2'
      ,'BUSINESS_INTENT_NAME'
      ,'BOT_INTENT_NAME'
      ,'CONVERSATION_LEVEL'
      ,'ABANDONED_REASONS'
      ,'TRANSFER_REASONS'
      ,'CONTAINED_REASONS'
      ,'ESCALATION_REASONS'
      ,'RESPONSE_GROUP'  
      ,'ANNOTATOR'
      ,'REVIEWED_BY'
      ,'REVIEWER_AGREE_ON_FIRST'
      ,'EST_CONVERSATION_LEVEL_FLAG'
      ,'TRANSFER_TO_AGENT'
      ,'TRANSFER_REASON'
      ,'IS_ANNOTATED'])



def build_bot_chat(response_string):
  htstring = """<li class=\"bot\" style=\"display:inline-block;clear:both;padding:20px;border-radius:30px;margin-bottom:2px;font-family:Helvetica, Arial, sans-serif;background:#eee;float:left;border-bottom-left-radius:5px;max-width:400px\">""" + response_string + """</li>"""
  #htstring += """<li class=\"bot\" style=\"display:inline-block;clear:both;padding:5px;border-radius:0px;margin-bottom:2px;font-family:Helvetica, Arial, sans-serif;background:#eee;float:left;border-bottom-left-radius:5px;max-width:400px\">""" + """Rasa Intent: """ + bot_intent + """</li>"""
  return htstring


def build_user_chat(user_string):
  return """<li class=\"user\" style=\"display:inline-block;clear:both;padding:20px;border-radius:30px;margin-bottom:2px;font-family:Helvetica, Arial, sans-serif;background:#0084ff;float:right;color:#fff;border-bottom-right-radius:5px;max-width:400px\">""" + user_string + """</li>"""

@st.cache()
def get_load_as_of_date():
  query = """select max(CAST(B.LOAD_DATE as DATE)) as MAX_DATE
      from TEAM_TECH_CONVOAI_P.CORE.TBL_BASE_CHAT_LOG_D B
      ;"""
  sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
  sc.create_cursor()
  df = sc.query_data(query)
  load_as_of_date = df['MAX_DATE'].max()
  sc.cursor.close()
  return load_as_of_date

@st.cache(allow_output_mutation=True)
def get_table_data(query): 
  sc = db_connection.snowflake_connector(userid=userid, 
                      account=account,
                      warehouse=warehouse, 
                      database=database, 
                      schema=schema)
  sc.create_cursor() 
  
  df = sc.update_table(query)
  
  sc.cursor.close()
  return df
  
#@st.cache(allow_output_mutation=True)
def query_data(start_date, end_date, show_annotated): 
    sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
    sc.create_cursor()
    # pulling all conversations including annotated and not annotated.
    query = f'''
              SELECT distinct
                a.CONVERSATION_TURN_KEY  as CONVERSATION_TURN_KEY
                ,a.CONVERSATION_ID  
                ,a.TURN_INDEX as TURN_INDEX
                ,cast(a.LOAD_DATE as date) as LOAD_DATE
                ,a.TIMESTAMP_OF_USER_TEXT
                , a.USER_UTTERANCE
                ,(a.BOT_RESPONSE ) as BOT_RESPONSE
                ,b.intent_business_groups as BUSINESS_INTENT_NAME
                ,a.name as BOT_INTENT_NAME
                ,a.CONFIDENCE
                ,a.TRANSFERRED_TO_LIVE_AGENT as TRANSFER_TO_AGENT
                ,a.REASON_FOR_TRANSFER as TRANSFER_REASON
                ,(a.est_conversation_level) as CONVERSATION_LEVEL
                ,replace(a.est_if_abandoned, 'nan', '') as ABANDONED_REASONS
                ,case
                    when (a.transferred_to_live_agent = TRUE)  
                    and ( substr(lower(a.USER_UTTERANCE), 1, 5) = '/oos_') then 'designed_transfer'
                    when (final_est_intent_flag = 'Est Known Intent') then 'known intent'
                    when (final_est_intent_flag = 'Est Unknown Intent') then 'unknown intent'
                    when (final_est_intent_flag = 'Escalation') then 'escalation'
                    else replace(est_if_transferred, 'nan', '')
                    end as TRANSFER_REASONS
                ,replace(a.est_if_contained, 'nan', '') as CONTAINED_REASONS
                ,replace(a.EST_ESCALATION_REASON, 'nan', '') as ESCALATION_REASONS
                ,a.EST_RESPONSE_ACCURACY as RESPONSE_GROUP  
                ,REGEXP_REPLACE(c.Annotator,'( ){1,}','') as Annotator
                ,REGEXP_REPLACE(c.REVIEWED_BY,'( ){1,}','') as REVIEWED_BY
                ,'' as reviewer_agree_on_first
                ,a.EST_CONVERSATION_LEVEL_FLAG
                ,case
                    when (c.Annotator is null) then False
                    else True
                    end as IS_ANNOTATED
        from TEAM_TECH_CONVOAI_P.CORE.TBL_BASE_CHAT_LOG_D a
        LEFT OUTER JOIN  TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_BUSINESS_INTENT_NAMES b
        ON (a.name = b.intent)
        LEFT OUTER JOIN  TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_TEST_RG c
        ON (a.CONVERSATION_TURN_KEY = c.CONVERSATION_TURN_KEY)
        Where   CAST(a.LOAD_DATE as DATE) between '{start_date}' 
          and  '{end_date}'
        '''
    if not show_annotated:
      query += f''' and a.conversation_id in 
      (select distinct conversation_id 
        from TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_TEST_RG
        where annotator = ''); '''
    else:
      query += f''';'''
      
    df = sc.query_data(query)
    df = df.sort_values(['CONVERSATION_ID', 'TIMESTAMP_OF_USER_TEXT'])
    df['TIMESTAMP_OF_USER_TEXT'] = pd.to_datetime(df['TIMESTAMP_OF_USER_TEXT'])
    df['USER_UTTER_DATE'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%Y-%m-%d')) 
    df['month_year'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%B-%Y')) 
    df['INTENT_NAME'] = df['BOT_INTENT_NAME'].str.lower().str.strip()
    #df['INTENT_NAME'] = df['BOT_INTENT_NAME'].str.strip()
    df['month_year'] = pd.Categorical(df['month_year'], ["February-2022", "March-2022", "April-2022", "May-2022", "June-2022","July-2022","August-2022","September-2022"
    ])
    df["CONVERSATION_LEVEL"] = df[['TRANSFER_TO_AGENT', 'CONVERSATION_LEVEL']].apply(lambda x: ut.check_trnfer_agent(x[0] , x[1] ), axis=1 )
    df["ABANDON_REASONS"] = df[['TRANSFER_TO_AGENT', 'ABANDONED_REASONS']].apply(lambda x: ut.check_trnfer_agent_abandon(x[0] , x[1] ), axis=1 )
    df["CONVERSATION_LEVEL"] = df[['CONVERSATION_LEVEL']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )
    df["USER_UTTERANCE"] = df[['USER_UTTERANCE']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )
    
    df["BOT_RESPONSE"] = df[['BOT_RESPONSE']].apply(lambda x: ut.check_remove_nan(x[0]), axis=1 )
    df["BOT_RESPONSE2"] = df[['BOT_RESPONSE']].apply(lambda x: ut.check_remove_buttons_text(x[0]), axis=1 )
    
    sc.cursor.close()
    return df
@st.cache()
def convo_exist(CONVO): 
      
      sc = db_connection.snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      query = f'''SELECT count(c.CONVERSATION_TURN_KEY)  as CONVERSATION_COUNT
        from TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_TEST_RG c
        where c.CONVERSATION_TURN_KEY = '{CONVO}';'''
      sc.create_cursor()
      df = sc.query_data(query)
  
      sc.cursor.close()
      return df['CONVERSATION_COUNT'].max()
    
@st.experimental_singleton
def get_business_intents(): 
      
      sc = db_connection.snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      query = f'''select distinct intent_business_groups as INTENT_BUSINESS_GROUPS, 
      intent as RASA_INTENTS 
      FROM TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_BUSINESS_INTENT_NAMES;'''
      sc.create_cursor()
      df = sc.query_data(query)
  
      sc.cursor.close()
      return df

def update_table_data(query): 
      t0 = time.time()
      sc = db_connection.snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      #sc.create_cursor(environment=environment, id_rsa_filename=id_rsa_filename, passcode_filename=passcode_filename, userid=userid) 
      sc.create_cursor()
      df = sc.update_table(query)
  
      print(f"INFO: Updating Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
      sc.cursor.close()
      return df
    
@st.cache(allow_output_mutation=True)
def build_update_str(TBL_Name, 
      INDEX,
      INTENT_BUSINESS_NAME, 
      INTENT_NAME, 
      CONVERSATION_LEVEL,
      ABANDONED_REASONS,
      TRANSFER_REASONS, 
      CONTAINED_REASONS, 
      ESCALATION_REASON, 
      ANNOTATOR, 
      REVIEWED_BY, 
      RESPONSE_GROUP,
      REVIEWER_AGREE_ON_FIRST,
      CONVERSATION_TURN_KEY):
    qstring = f'''update {TBL_Name} 
            set INTENT_BUSINESS_NAME = '{INTENT_BUSINESS_NAME}'
            ,INTENT_NAME = '{INTENT_NAME}'
            ,CONVERSATION_LEVEL = '{CONVERSATION_LEVEL}'
            ,ABANDONED_REASONS = '{ABANDONED_REASONS}'
            , TRANSFER_REASONS = '{TRANSFER_REASONS}'
            ,CONTAINED_REASONS = '{CONTAINED_REASONS}'
            ,ESCALATION_REASON = '{ESCALATION_REASON}'
            ,ANNOTATOR = '{ANNOTATOR}'
            ,REVIEWED_BY = '{REVIEWED_BY}'
            ,ANNOTATED_RESPONSE_GROUP = '{RESPONSE_GROUP}'
            ,REVIEWER_AGREE_ON_FIRST = '{REVIEWER_AGREE_ON_FIRST}'
            where CONVERSATION_TURN_KEY = '{CONVERSATION_TURN_KEY}';'''
    return qstring
  
#-----------------------------------End of functions-------------------------------
df1 = get_table_data(xqc)
df2 = get_business_intents()

business_intent_dict = {}
for i in df2["INTENT_BUSINESS_GROUPS"].tolist(): 
  business_intent_dict[i] = df2[df2["INTENT_BUSINESS_GROUPS"]==i]["RASA_INTENTS"].tolist()

#------------------------------------------------------------------
# Initialization session variables
if 'runme' not in st.session_state:
    st.session_state['runme'] = 0

if 'INTENT_BUSINESS_GROUPS' not in st.session_state:
    sorted_df = df2.sort_values(by='INTENT_BUSINESS_GROUPS')
    st.session_state['INTENT_BUSINESS_GROUPS'] = sorted_df['INTENT_BUSINESS_GROUPS'] .unique().tolist()
    
if 'BOT_INTENT_NAME' not in st.session_state:
    sorted_df = df2.sort_values(by='RASA_INTENTS')
    st.session_state['BOT_INTENT_NAME'] = sorted_df['RASA_INTENTS'] .unique().tolist()

if 'RASA_INTENTS' not in st.session_state:
    st.session_state['RASA_INTENTS'] = df2['RASA_INTENTS'].unique()
    
# --- Initialising run query ---
if "run_query" not in st.session_state:
     st.session_state.run_query = False
   
if "run_update_query" not in st.session_state:
     st.session_state.run_update_query = False
     
def form_callback():
    st.write(st.session_state.my_slider)
    st.write(st.session_state.my_checkbox)

def set_runme_on():
  st.session_state.runme = 1

load_as_of_date = get_load_as_of_date()
load_date_start_week = load_as_of_date - timedelta(days = 2)
dval = pd.to_datetime(load_date_start_week, format="%Y-%m-%d")

def set_start_date():
  st.session_state['start_date'] = pd.to_datetime(load_date_start_week, format="%Y-%m-%d")
  dval = pd.to_datetime(load_date_start_week, format="%Y-%m-%d")

def set_end_date():
  st.session_state['end_date'] = pd.to_datetime(load_date_start_week, format="%Y-%m-%d")
  
if 'start_date' not in st.session_state:
    st.session_state['start_date'] = pd.to_datetime(load_as_of_date, format="%Y-%m-%d")

if 'set_checkbox' not in st.session_state:
    st.session_state['set_checkbox'] = False
    
if 'sdate1' not in st.session_state:
    st.session_state['sdate1'] = pd.to_datetime(load_as_of_date, format="%Y-%m-%d")

if 'edate1' not in st.session_state:
    st.session_state['edate1'] = pd.to_datetime(load_as_of_date, format="%Y-%m-%d")
    
def set_checkbox():
  reload_data_option = st.session_state['checkbox_key'] 
  
if 'end_date' not in st.session_state:
    st.session_state['end_date'] = pd.to_datetime(load_as_of_date, format="%Y-%m-%d")

if 'formBtn' not in st.session_state:
    st.session_state['formBtn'] = False


if st.session_state['authentication_status']:
    #st.write('Welcome *%s*' % (st.session_state['name']))
    st.title(f'''Let's Annotate''') 
elif st.session_state['authentication_status'] == False:
    st.error('Username/password is incorrect')
elif st.session_state['authentication_status'] == None:
    st.warning('Please enter your username and password')
    

#reload_data_option = st.checkbox("Set reload_data as true on next app refresh.", value=False, key='btnReload')
#use_fixed_key = st.checkbox("Use fixed key in AgGrid call", value=False)
#if use_fixed_key:
key="'an_unique_key'"
#else:
#key=None
    

if st.session_state['authentication_status']:
  with st.form(key="form"):
    #if st.session_state.run_query:
    col1, col2,_ = st.columns([2,2, 2])
    with col1:
       st.date_input("Start Date",  key="sdate1")
       ConvoToAnnotate = st.number_input("Conversations per group", min_value=1, value=1)
    with col2:
       st.date_input("End Date",  key="edate1")
       show_annotated = st.checkbox("Show previously annotated data", value=False)
    #with col3: 
      
    submit_button = st.form_submit_button(label="Select Dates")
    if submit_button:
      st.session_state['formBtn']=True
    #st.write(st.session_state['formBtn'])
    st.write(st.session_state)
    if st.session_state['formBtn']:
      st.markdown("Running Query")
      try:
        all_data = query_data(st.session_state['sdate1'], st.session_state['edate1'], show_annotated)
        all_data =  all_data[[ 'CONVERSATION_TURN_KEY'
          ,'CONVERSATION_ID'  
          ,'TURN_INDEX'
          ,'USER_UTTER_DATE'
          ,'USER_UTTERANCE'
          ,'BOT_RESPONSE'
          ,'BOT_RESPONSE2'
          ,'BUSINESS_INTENT_NAME'
          ,'BOT_INTENT_NAME'
          ,'CONVERSATION_LEVEL'
          ,'ABANDONED_REASONS'
          ,'TRANSFER_REASONS'
          ,'CONTAINED_REASONS'
          ,'ESCALATION_REASONS'
          ,'RESPONSE_GROUP'  
          ,'ANNOTATOR'
          ,'REVIEWED_BY'
          ,'REVIEWER_AGREE_ON_FIRST'
          ,'EST_CONVERSATION_LEVEL_FLAG'
          ,'TRANSFER_TO_AGENT'
          ,'TRANSFER_REASON'
          ,'IS_ANNOTATED']]
      except ValueError: 
        all_data = pd.DataFrame(columns = [ 'CONVERSATION_TURN_KEY'
          ,'CONVERSATION_ID'  
        ,'TURN_INDEX'
        ,'USER_UTTER_DATE'
        ,'USER_UTTERANCE'
        ,'BOT_RESPONSE'
        ,'BUSINESS_INTENT_NAME'
        ,'BOT_INTENT_NAME'
        ,'BOT_RESPONSE2'
        ,'CONVERSATION_LEVEL'
        ,'ABANDONED_REASONS'
        ,'TRANSFER_REASONS'
        ,'CONTAINED_REASONS'
        ,'ESCALATION_REASONS'
        ,'RESPONSE_GROUP'  
        ,'ANNOTATOR'
        ,'REVIEWED_BY'
        ,'REVIEWER_AGREE_ON_FIRST'
        ,'EST_CONVERSATION_LEVEL_FLAG'
        ,'TRANSFER_TO_AGENT'
        ,'TRANSFER_REASON'
        ,'IS_ANNOTATED'])
      #st.markdown("Select dates to begin annotation **Click** ❄️")

      #st.sidebar.subheader("Tool Filters")
      #Date calendar pricker to populate DFs, Weekly, YTD
      today = date.today() 
      tomorrow = date.today() + timedelta(days = 1)
      
      start_date = st.session_state['sdate1'] 
      #st.sidebar.date_input("Start Date", value=pd.to_datetime("2022-09-01", format="%Y-%m-%d"), key="sdate2")
      end_date = st.session_state['edate1'] 
      #st.sidebar.date_input("End Date", value=pd.to_datetime("today", format="%Y-%m-%d"), key="edate2")
      
      # convert the dates to string
      start = start_date.strftime("%Y-%m-%d")
      end = end_date.strftime("%Y-%m-%d")

      #df = all_data.query("EST_CONVERSATION_LEVEL_FLAG == @FirstFilter & (USER_UTTER_DATE >= @start & USER_UTTER_DATE <= @end)")
      df = all_data.query("(USER_UTTER_DATE >= @start & USER_UTTER_DATE <= @end)")
      #st.dataframe(df)
      
    else:
      st.markdown("You did not click on submit button.")
      st.stop()
      
col1, col2 = st.columns([5,3])
#col1,col2,_ = st.columns([1,2,1])
with col1:
  #st.write(intentList)
  with st.form("my_form"):
    #Infer basic colDefs from dataframe types
    gb = GridOptionsBuilder.from_dataframe(df)
    
    
    #customize gridOptions
    #gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc='sum', editable=True)
    gb.configure_column("USER_UTTER_DATE", type=["dateColumnFilter","customDateTimeFormat"],  editable='false', custom_format_string='yyyy-MM-dd', pivot=True)
    
    gb.configure_pagination(enabled=True, paginationAutoPageSize=False, paginationPageSize=ConvoToAnnotate)
    #gb.configure_default_column(editable=True, groupable=True)
    #sel_mode = st.radio('Selection Type', options=['single', 'multiple'])
    sel_mode = 'multiple'
    #customize gridOptions
    gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc='', editable=True)
    
    #configures last row to use custom styles based on cell's value, injecting JsCode on components front end
    cellsytle_jscode = JsCode("""
    function(params) {
      if (params.value == 'true') {
          return {
              'color': 'white',
              'backgroundColor': 'darkred'
          }
      } else {
          return {
              'color': 'black',
              'backgroundColor': 'white'
          }
      }
    };
    """)

    abandoned_reasons_js = JsCode("""
    function(params) {
      var selectedConversationLevel = params.data.CONVERSATION_LEVEL;
      if (selectedConversationLevel === 'Abandoned') {
          return {
              'values': ['', 'Greeting', 'Intent']
          }
      } else {
          return {
              'values': ['']
          }
      }
    };
    """)

    contained_reasons_js = JsCode("""
    function(params) {
      var selectedConversationLevel = params.data.CONVERSATION_LEVEL;
      if (selectedConversationLevel === 'Abandoned') {
          return {
              'values': ['', 'Partial Containment']
          }
      } else if (selectedConversationLevel === 'Contained') {
          return {
              'values': ['Full Containment']
          }
      } else if (selectedConversationLevel === 'Transferred') {
          return {
              'values': ['', 'Partial Containment']
          }
      } else {
          return {
              'values': ['']
          }
      }
    };
    """)

    utterance_type_js = JsCode("""
    function(params) {
      var selectedConversationLevel = params.data.CONVERSATION_LEVEL;
      if (selectedConversationLevel === 'Abandoned') {
          return {
              'values': ['known intent','unknown intent','escalation','sentiment']
          }
      } else if (selectedConversationLevel === 'Contained') {
          return {
              'values': ['known intent','unknown intent']
          }
      } else if (selectedConversationLevel === 'Transferred') {
          return {
              'values': ['known intent','unknown intent','escalation','sentiment','designed_transfer']
          }
      } else {
          return {
              'values': ['known intent','unknown intent','escalation','sentiment','designed_transfer']
          }
      }
    };
    """)

    escslation_reasons_js = JsCode("""
    function(params) {
      var selectedTransferReason = params.data.TRANSFER_REASONS;
      if (selectedTransferReason === 'escalation') {
          return {
              'values': ['Greeting','Intent']
          }
      } else {
          return {
              'values': ['']
          }
      }
    };
    """)

    bus_intent_str = "function(params) { var selectedBusinessIntentName = params.data.BUSINESS_INTENT_NAME;"
    len_dict = len(business_intent_dict)
    i = 0
    #st.write(business_intent_dict)
    for key, value in business_intent_dict.items():
      if i == 0:
        bus_intent_str += f"if (selectedBusinessIntentName === '{key}') {{ return {{ 'values': {value} }} }}"
      else:
        bus_intent_str += f"else if (selectedBusinessIntentName === '{key}') {{ return {{ 'values': {value} }} }}"
      i += 1
    bus_intent_str += "};"

    bus_intent_js = JsCode(bus_intent_str)

    
    #highlight cell on change JS
    js = JsCode("""
    function(e) {
      let api = e.api;
      let rowIndex = e.rowIndex;
      let col = e.column.colId;
    
      let rowNode = api.getDisplayedRowAtIndex(rowIndex);
      api.flashCells({
        rowNodes: [rowNode],
        columns: [col],
        flashDelay: 10000000000
      });
    
    };
    """)
  
    #gb.gridOptions['getRowStyle'] = jscode2    
    gb.configure_column("RESPONSE_GROUP", 
        cellStyle=cellsytle_jscode,
        headerName='Response Group')
    
    #Select column select values
    gb.configure_column('CONVERSATION_LEVEL',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':[
                      '',
                      'Abandoned',
                      'Contained',
                      'Transferred',
                      'Known data Issue']},
      headerName='Conversation Level',
      cellEditorPopup=True
    )
    gb.configure_column('ABANDONED_REASONS',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams= abandoned_reasons_js,
      headerName='Abandoned Reasons',
      cellEditorPopup=True
    )
    gb.configure_column('ESCALATION_REASONS',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams=escslation_reasons_js,
      headerName='Escalation Reasons',
      cellEditorPopup=True
    )
    gb.configure_column('TRANSFER_REASONS', 
      cellEditor='agRichSelectCellEditor',
      cellEditorParams=utterance_type_js,
      cellEditorPopup=True,
      headerName='Utterance Type',
      headerTooltip='Is our Chat Bot trained to handle the user requested task(s) **(known/Unknown)**'
    )
    gb.configure_column('CONTAINED_REASONS',  
      cellEditor='agRichSelectCellEditor',
      cellEditorParams= contained_reasons_js,
      headerName='Contained Reasons',
      cellEditorPopup=True
    )
    gb.configure_column('RESPONSE_GROUP',  
      cellEditor='agRichSelectCellEditor', 
      cellEditorParams={'values':['improper response',
                      'proper response']},
      cellEditorPopup=True,
      headerName='Annotator Response Group',
      singleClickEdit=True
    )
    gb.configure_column('ANNOTATOR',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':[
                      '',st.session_state['username']
                      ]},
      cellEditorPopup=True,
      headerName='Annotator',
      singleClickEdit=True
    )
    gb.configure_column('REVIEWED_BY',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams={'values':[
                      '',
                      'PZLYH9',
                      'TZYTT7',
                      'KZ60Z3',
                      'TZX8QT',
                      'PZSW95',
                      'QZ6JS1',
                      'RZ383S',
                      'xz47sx']},
      cellEditorPopup=True,
      headerName='Reviewed By',
      headerTooltip='The annotator reviewing the conversation during the cross-validation process.',
      singleClickEdit=True
    )
    gb.configure_column('REVIEWER_AGREE_ON_FIRST',
      cellEditor='agRichSelectCellEditor',
      headerName='Reviewer Agreement',
      cellEditorParams={'values':['','Y','N']},
      cellEditorPopup=True
    )
    gb.configure_column('CONVERSATION_TURN_KEY',
      #cellEditor='agRichSelectCellEditor',
      #cellEditorParams={'values':['','Y','N']},
      #filter: 'agTextColumnFilter',
      editable=False,
      headerName='Select Records',
      headerTooltip='Select checkbox to begin annotation.',
      #cellStyle=cellsytle_jscode
    )
    gb.configure_column('CONVERSATION_ID',
       editable=False,
       filter=True,
       rowGroup=True, # We need to purchase an enterprise lic
       hide=True,
       headerName='Conversation Id'
    ) 
    gb.configure_column('TURN_INDEX',
       editable=False,
       width=10,
       filter=True,
       headerName='Turn Index',
       headerTooltip='Turn Index'
    )
    gb.configure_column('USER_UTTER_DATE',
       editable=False,
       width=100,
       headerName='Utter Date',
       headerTooltip='Date and time user interacted with the bot.'
    )
    
    gb.configure_column('USER_UTTERANCE',
       editable=False,
       filter=True,
       autoHeight=True,
       wrapText=True,
       width=400,
       headerName='User Utterance',
       headerTooltip='User input into the chat bubble.'
    )
    gb.configure_column('BOT_RESPONSE2',
       editable=False,
       hide=False,
       wrapText=True,
       width=400,
       headerName='Bot Response (parsed)',
       headerTooltip='The response given to the user by the bot.'
    )
    gb.configure_column('BOT_RESPONSE',
       editable=False,
       autoHeight=True,
       hide=True,
       wrapText=True,
       width=400,
       headerName='Bot Response',
       headerTooltip='The response given to the user by the bot.'
    )
    gb.configure_column('EST_CONVERSATION_LEVEL_FLAG',
       editable=False,
       hide=True
    )
    gb.configure_column('IS_ANNOTATED',
       editable=False,
       filter=True,
       headerName='Has Been Annotated',
       headerTooltip='Indicates if a utterance has been annotated.'
    ) 
     
    gb.configure_column('BUSINESS_INTENT_NAME',
     cellEditor='agRichSelectCellEditor',
     #cellEditorParams={'values':[
     #                '',
     #              '1099', 'Additional Account Owner', 'ATM', 'Bill Pay', 'Checks and Deposit Money', 'Deposit FAQs and CS Searches', 'Money Transfers', 'Product Pages', 'Profile and Settings', 'Rates', 'Statements and Forms', 'Savings Toolkit', 'Small Talk', 'Wire Transfer', 'Zelle', 'Agent Transfer', 'Covid', 'Ally FAQs', 'Out Of Scope', 'untrained', 'Conversation Flow Control', 'Checks, Certified and Money Orders', 'unknown']},
     cellEditorParams={'values': st.session_state['INTENT_BUSINESS_GROUPS'] }, 
     headerName='Business Intent',
     headerTooltip='Intent used in reporting rollups.',
     cellEditorPopup=True,
     singleClickEdit=True
    )
    gb.configure_column('BOT_INTENT_NAME',
      cellEditor='agRichSelectCellEditor',
      cellEditorParams=bus_intent_js,
      cellEditorPopup=True,
      headerName='Rasa Intent',
      headerTooltip='The intent predicted ny the RASA NLU engine.',
      singleClickEdit=True
    )
    gb.configure_column('TRANSFER_TO_AGENT',
      editable=False,
      filter=True,
      #cellStyle=jscode2,
      headerName='Transfer to Agent',
      headerTooltip='Indicates a conversation transferred to an agent.'
    )
    
    gb.configure_grid_options(enableRangeSelection=True, onCellValueChanged=js, rowGroupPanelShow='onlyWhenGrouping')
    
    gb.configure_side_bar()
    gb.configure_selection(selection_mode=sel_mode, use_checkbox=True)
    gridoptions = gb.build()
    
    #if use_fixed_key:
    grid_table = AgGrid(df, gridOptions=gridoptions,
                      update_mode=GridUpdateMode.SELECTION_CHANGED | GridUpdateMode.VALUE_CHANGED,
                      #| GridUpdateMode.SELECTION_CHANGED,
                      # | GridUpdateMode.VALUE_CHANGED,
                      #|GridUpdateMode.MANUAL,
                      #
                      height=400,
                      allow_unsafe_jscode=True,
                      #fit_columns_on_grid_load=True,
                      # enable_enterprise_modules = True,
                      enable_enterprise_modules=True, 
                      license_key=license_key,
                      theme='streamlit',
                      key='aggrid1',
                      reload_data=False
                      )
    
    submitted = st.form_submit_button("Review Selection")
    #st.stop
    if submitted:
      df_selected = pd.DataFrame(columns = [ 'CONVERSATION_ID'  
                                ,'TURN_INDEX'
                                ,'BUSINESS_INTENT_NAME'
                                ,'BOT_INTENT_NAME'
                                ,'CONVERSATION_LEVEL'
                                ,'ABANDONED_REASONS'
                                ,'TRANSFER_REASONS'
                                ,'CONTAINED_REASONS'
                                ,'ESCALATION_REASONS'
                                ,'RESPONSE_GROUP'  
                                ,'ANNOTATOR'
                                ,'REVIEWED_BY'
                                ,'REVIEWER_AGREE_ON_FIRST'
                                ])
                            
    sel_row = grid_table["selected_rows"]
    total_utterance = f"Total number of conversations: {len(pd.unique( df['CONVERSATION_ID'] ) )}, Total number of user utterances: {len(df)}"
    st.write(total_utterance)
    
    st.subheader("Review Changes")
    #st.write(sel_row)
    
    try:
    # TODO: write code...
      df_selected = pd.DataFrame(sel_row)
      df_selected.columns = df_selected.columns.str.upper()
      if df_selected.empty == False:
        df_sel = df_selected[[ 'CONVERSATION_ID'  
                                ,'TURN_INDEX'
                                ,'BUSINESS_INTENT_NAME'
                                ,'BOT_INTENT_NAME'
                                ,'CONVERSATION_LEVEL'
                                ,'ABANDONED_REASONS'
                                ,'TRANSFER_REASONS'
                                ,'CONTAINED_REASONS'
                                ,'ESCALATION_REASONS'
                                ,'RESPONSE_GROUP'  
                                ,'ANNOTATOR'
                                ,'REVIEWED_BY'
                                ,'REVIEWER_AGREE_ON_FIRST'
                                ]]
      #st.write(df_selected.columns)
        st.dataframe(df_sel)
    except IndexError:
      df_selected = pd.DataFrame()
      #st.write(df_selected.empty)
      clickit = False
    
with col2:
  try:      
    if df_selected.empty == False:
      #st.write('helllllo')
      html_chat_string = """<ul style=\"list-style:none;margin:0;padding:0;width:500px;\">"""
      for i, r in df_selected.iterrows():
        html_chat_string += build_bot_chat("Conversation ID: " + r['CONVERSATION_TURN_KEY'])
        bot_reponses = ut.check_remove_buttons_text(r['BOT_RESPONSE'])
        html_chat_string += build_user_chat(r['USER_UTTERANCE'])
        for bot_resp in bot_reponses:
          html_chat_string += build_bot_chat(bot_resp)
          html_chat_string += """</ul>"""
        html_chat_string += build_bot_chat("Rasa Intent: " + r['BOT_INTENT_NAME'])
      st.markdown(html_chat_string,unsafe_allow_html=True)
      
      if st.button('Update db', key='btnUpdate'):
        selected_data = (df_selected[['BUSINESS_INTENT_NAME', 'BOT_INTENT_NAME', 'CONVERSATION_LEVEL', 'ABANDONED_REASONS', 'CONTAINED_REASONS', 'TRANSFER_REASONS', 'ESCALATION_REASONS']].copy()).values.tolist()
        qa_results = QA.QualityAssurance(selected_data)
        overall_qa_result = qa_results.get_qa_result()
        if overall_qa_result:
          for i, r in df_selected.iterrows():
            ustring = build_update_str('TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG', i, 
            r['BUSINESS_INTENT_NAME'],
            r['BOT_INTENT_NAME'], 
            r['CONVERSATION_LEVEL'],
            r['ABANDONED_REASONS'],
            r['TRANSFER_REASONS'], 
            r['CONTAINED_REASONS'], 
            r['ESCALATION_REASONS'], 
            r['ANNOTATOR'], 
            r['REVIEWED_BY'], 
            r['RESPONSE_GROUP'],
            r['REVIEWER_AGREE_ON_FIRST'],
            r['CONVERSATION_TURN_KEY'] )
            
            co = convo_exist(r['CONVERSATION_TURN_KEY'])
            #st.write(co)
    
            #st.write(ustring)
            if df_selected.empty == False:
              st.session_state.runme = 0
              #uD = update_table_data(ustring)
              #st.write('helllllo 33333')
            #st.write('##### Updated db')
            #df_update = pd.read_sql('SELECT * FROM product', con=conn)
            #st.write(uD.count())
        else:
          if not qa_results.get_valid_convo():
            st.write("QA Failed on a conversation level")
          else:
            st.write("QA Failed on turn: ", str(qa_results.get_failed_turn()+1))
            if not qa_results.get_valid_intent():
              st.write("Invalid combinations of intent labels.")
            else:
              st.write("Invalid combination of conversation_level, abandon_reason, contained_reason, utterance_type, and escalation_reason")
            
  except NameError: 
    st.write('test mode')

submitted=False
submit_button=False
