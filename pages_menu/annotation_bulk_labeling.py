from distutils import errors
from distutils.log import error
import streamlit as st
import random
import plotly.express as px
import pandas as pd 
import numpy as np
import altair as alt
from itertools import cycle
import yfinance as yf
import matplotlib.pyplot as plt

from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode, JsCode
import streamlit as st 
import snowflake_con.db_connection as db_connection 
from st_aggrid import AgGrid, GridUpdateMode
from st_aggrid.grid_options_builder import GridOptionsBuilder
import time 
import pandas as pd 

import pathlib 
import numpy as np
import pandas as pd
import ipywidgets as widgets

from whatlies import EmbeddingSet 
from whatlies.transformers import Pca, Umap
from hulearn.preprocessing import InteractivePreprocessor
from hulearn.experimental.interactive import InteractiveCharts
from whatlies.language import UniversalSentenceLanguage, LaBSELanguage

import seaborn as sns 

np.random.seed(42)
#st.markdown("# Bulk Labeling Tool ❄️")
st.markdown("Bulk Labeling Tool")
st.sidebar.markdown("# Bulk Labeling Tool ❄️")


# annotated data date range
st.header("")
col1, col2 = st.columns(2)
with col1:
  a_start_date = st.date_input("Start Date", value=pd.to_datetime("today", format="%Y-%m-%d"), key="sdate1")
with col2:
  a_end_date = st.date_input("End Date", value=pd.to_datetime("today", format="%Y-%m-%d"), key="edate1")


# snowflake NLU files Config - revise to your settings 
userid="PZLYH9"
account='ally.us-east-1.privatelink'
warehouse='WH_TEAM_TECH_CONVOAI_ME' 
database='TEAM_TECH_CONVOAI_P'
schema='CORE'
# running environment: local, prod, workbench 
environment='prod'
id_rsa_filename="id_rsa" 
passcode_filename='id_rsa_passcode'

@st.experimental_memo(show_spinner=True, suppress_st_warning=True)

def query_data(start_date, end_date): 
    t0 = time.time()
    sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
    sc.create_cursor() 
    # pulling all conversations including annotated and not annotated.
    query = f'''
        select B.*, A.risk_error_type_i, A.risk_error_type_ii, A.risk_error_type_iii
        from TEAM_TECH_CONVOAI_P.CORE.TBL_FACT_CHAT_COVERAGE A,
        TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG B
        where A.CONVERSATION_TURN_KEY = B.CONVERSATION_TURN_KEY
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) >= CAST('{start_date}' as DATE))
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) <= CAST('{end_date}' as DATE))
        order by B.CONVERSATION_ID,B.TIMESTAMP_OF_USER_TEXT  
        '''
    ## and B.EXPECTED_ANNOTATION = TRUE
    #st.write(query)
    df = sc.query_data(query)

    # annotated_df = annotated_df[annotated_df.ANNOTATOR!='']
    df = df.sort_values(['CONVERSATION_ID', 'TIMESTAMP_OF_USER_TEXT'])
    df['TIMESTAMP_OF_USER_TEXT'] = pd.to_datetime(df['TIMESTAMP_OF_USER_TEXT'])
    df['MONTH_YEAR'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%B-%Y')) 
    df['INTENT_NAME'] = df['INTENT_NAME'].str.lower()
    df['INTENT_NAME'] = df['INTENT_NAME'].str.strip()
    df['MONTH_YEAR'] = pd.Categorical(df['MONTH_YEAR'], ["February-2022", "March-2022", "April-2022", "May-2022", "June-2022","July-2022","August-2022","September-2022"
    ])
    print(f"INFO: Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
    sc.cursor.close()
    df.columns = df.columns.str.upper()
    return df
@st.cache(allow_output_mutation=True)
def build_update_str(TBL_Name, 
      INDEX,
      INTENT_BUSINESS_NAME, 
      INTENT_NAME, 
      CONVERSATION_LEVEL,
      ABANDONED_REASONS,
      TRANSFER_REASONS, 
      CONTAINED_REASONS, 
      ESCALATION_REASON, 
      ANNOTATOR, 
      REVIEWED_BY, 
      RESPONSE_GROUP,
      REVIEWER_AGREE_ON_FIRST,
      CONVERSATION_TURN_KEY):
  qstring = f'''update {TBL_Name} 
            set INTENT_BUSINESS_NAME = '{INTENT_BUSINESS_NAME}'
            ,INTENT_NAME = '{INTENT_NAME}'
            ,CONVERSATION_LEVEL = '{CONVERSATION_LEVEL}'
            ,ABANDONED_REASONS = '{ABANDONED_REASONS}'
            , TRANSFER_REASONS = '{TRANSFER_REASONS}'
            ,CONTAINED_REASONS = '{CONTAINED_REASONS}'
            ,ESCALATION_REASON = '{ESCALATION_REASON}'
            ,ANNOTATOR = '{ANNOTATOR}'
            ,REVIEWED_BY = '{REVIEWED_BY}'
            ,ANNOTATED_RESPONSE_GROUP = '{RESPONSE_GROUP}'
            ,REVIEWER_AGREE_ON_FIRST = '{REVIEWER_AGREE_ON_FIRST}'
            where CONVERSATION_TURN_KEY = '{CONVERSATION_TURN_KEY}';'''
  return qstring

def update_table_data(query): 
  t0 = time.time()
  sc = db_connection.snowflake_connector(userid=userid, 
                          account=account,
                          warehouse=warehouse, 
                          database=database, 
                          schema=schema)
  #sc.create_cursor(environment=environment, id_rsa_filename=id_rsa_filename, passcode_filename=passcode_filename, userid=userid) 
  sc.create_cursor()
  df = sc.update_table(query)

  print(f"INFO: Updating Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
  sc.cursor.close()
  return df
# get the chat data
all_data = query_data(a_start_date, a_end_date)

# If you want to use another dataset this is where you should define a new list of texts.
txt = pathlib.Path("nlu.md").read_text()
texts = list(set([t.replace(" - ", "") for t in txt.split("\n") if len(t) > 0 and t[0] != "#"]))
st.write(f"We're going to label {len(texts)} texts.")

# The language agnostic bert model works is a good starting option, 
# especially for Non-English use-cases but it is a fair bit slower.
# You can swap this out with another embedding source if you feel like though. 
# lang = LaBSELanguage()
lang = UniversalSentenceLanguage(variant="large")

# This is where we prepare all of the state
embset = lang[texts]
df = embset.transform(Umap(2)).to_dataframe().reset_index()
df.columns = ['text', 'd1', 'd2']
df['label'] = ''

# Here's the global state object
state = {}
state['df'] = df.copy()
state['chart'] = InteractiveCharts(df.loc[lambda d: d['label'] == ''], labels=['group'])

pd.set_option('display.max_colwidth', -1)

def show_draw_chart(b=None):
    with out_table:
        out_table.clear_output()
    with out_chart:
        out_chart.clear_output()
        state['chart'].dataf = state['df'].loc[lambda d: d['label'] == '']
        state['chart'].charts = []
        state['chart'].add_chart(x='d1', y='d2', legend=False)

def show_examples(b=None):
    with out_table:
        out_table.clear_output()
        tfm = InteractivePreprocessor(json_desc=state['chart'].data())
        subset = state['df'].pipe(tfm.pandas_pipe).loc[lambda d: d['group'] != 0]
        display(subset.sample(min(15, subset.shape[0]))[['text']])

def assign_label(b=None):
    tfm = InteractivePreprocessor(json_desc=state['chart'].data())
    idx = state['df'].pipe(tfm.pandas_pipe).loc[lambda d: d['group'] != 0].index
    state['df'].iloc[idx, 3] = label_name.value
    with out_counter:
        out_counter.clear_output()
        n_lab = state['df'].loc[lambda d: d['label'] != ''].shape[0]
        print(f"{n_lab}/{state['df'].shape[0]} labelled")

def retrain_state(b=None):
    keep = list(state['df'].loc[lambda d: d['label'] == '']['text'])
    umap = Umap(2)
    new_df = EmbeddingSet(*[e for e in embset if e.name in keep]).transform(umap).to_dataframe().reset_index()
    new_df.columns = ['text', 'd1', 'd2']
    new_df['label'] = ''
    state['df'] = pd.concat([new_df, state['df'].loc[lambda d: d['label'] != '']])
    show_draw_chart(b)
    
out_table = widgets.Output()
out_chart = widgets.Output()
out_counter = widgets.Output()

label_name = widgets.Text("label name")

btn_examples = widgets.Button(
    description='Show Examples',
    icon='eye'
)

btn_label = widgets.Button(
    description='Add label',
    icon='check'
)

btn_retrain = widgets.Button(
    description='Retrain',
    icon='coffee'
)

btn_redraw = widgets.Button(
    description='Redraw',
    icon='check'
)

btn_examples.on_click(show_examples)
btn_label.on_click(assign_label)
btn_redraw.on_click(show_draw_chart)
btn_retrain.on_click(retrain_state)

show_draw_chart()
display(widgets.VBox([widgets.HBox([btn_retrain, btn_examples, btn_redraw]), 
                      widgets.HBox([out_chart, out_table])]), 
        label_name, 
        widgets.HBox([btn_label, out_counter]))
        

#######################################
""" @st.cache
def random_data():
	return random.sample(range(100), 50), random.sample(range(100), 50)

st.subheader("Plotly interactive scatterplot")
x, y = random_data()
fig = px.scatter(x=x, y=y, title="My fancy plot")
v = st_scatterplot(fig)
st.write(v)
"""
##################################33



st.title("Palmer's Penguins") 
st.markdown('Use this Streamlit app to make your own scatterplot about penguins!') 
 
selected_x_var = st.selectbox('What do want the x variable to be?', 
  ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g']) 
selected_y_var = st.selectbox('What about the y?', 
  ['bill_depth_mm', 'bill_length_mm', 'flipper_length_mm', 'body_mass_g']) 
 
penguin_file = st.file_uploader('Select Your Local Penguins CSV') 
if penguin_file is not None: 
	penguins_df = pd.read_csv(penguin_file) 
else: 
	st.stop()

sns.set_style('darkgrid')
markers = {"Adelie": "X", "Gentoo": "s", "Chinstrap":'o'}
fig, ax = plt.subplots() 
ax = sns.scatterplot(data = penguins_df, x = selected_x_var, 
  y = selected_y_var, hue = 'species', markers = markers,
  style = 'species') 
plt.xlabel(selected_x_var) 
plt.ylabel(selected_y_var) 
plt.title("Scatterplot of Palmer's Penguins") 
st.pyplot(fig) 
  
  
