import streamlit as st 
import snowflake_con.db_connection as db_connection 
import time 
import pandas as pd 
#st.set_page_config(layout="wide",page_title="Report", page_icon="❄️")
st.markdown("# Data Quality Check ❄️")
st.sidebar.markdown("# Data Quality Check ❄️")


# annotated data date range
start_date = '2022-05-01'
end_date = '2022-09-30'
# snowflake NLU files Config - revise to your settings 
userid="PZLYH9"
account='ally.us-east-1.privatelink'
warehouse='WH_TEAM_TECH_CONVOAI_ME' 
database='TEAM_TECH_CONVOAI_P'
schema='CORE'
# running environment: local, prod, workbench 
environment='prod'
id_rsa_filename="id_rsa" 
passcode_filename='id_rsa_passcode'

@st.experimental_memo(show_spinner=True, suppress_st_warning=True)
def query_data(start_date, end_date): 
    t0 = time.time()
    sc = db_connection.snowflake_connector(userid=userid, 
                            account=account,
                            warehouse=warehouse, 
                            database=database, 
                            schema=schema)
    sc.create_cursor() 
    # pulling all conversations including annotated and not annotated.
    query = f'''
        select B.*, A.risk_error_type_i, A.risk_error_type_ii, A.risk_error_type_iii
        from TEAM_TECH_CONVOAI_P.CORE.TBL_FACT_CHAT_COVERAGE A,
        TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG B
        where A.CONVERSATION_TURN_KEY = B.CONVERSATION_TURN_KEY
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) >= CAST('{start_date}' as DATE))
        and ( CAST(B.TIMESTAMP_OF_USER_TEXT as DATE) <= CAST('{end_date}' as DATE))
        order by B.CONVERSATION_ID,B.TIMESTAMP_OF_USER_TEXT 
        '''
    ## and B.EXPECTED_ANNOTATION = TRUE
    df = sc.query_data(query)

    # annotated_df = annotated_df[annotated_df.ANNOTATOR!='']
    df = df.sort_values(['CONVERSATION_ID', 'TIMESTAMP_OF_USER_TEXT'])
    df['TIMESTAMP_OF_USER_TEXT'] = pd.to_datetime(df['TIMESTAMP_OF_USER_TEXT'])
    df['month_year'] = df['TIMESTAMP_OF_USER_TEXT'].apply(lambda x: x.strftime('%B-%Y')) 
    df['INTENT_NAME'] = df['INTENT_NAME'].str.lower()
    df['INTENT_NAME'] = df['INTENT_NAME'].str.strip()
    df['month_year'] = pd.Categorical(df['month_year'], ["February-2022", "March-2022", "April-2022", "May-2022", "June-2022","July-2022","August-2022","September-2022"
    ])
    print(f"INFO: Base Table Querying Running Time: {round(time.time()-t0, 3)}s!")
    sc.cursor.close()
    return df
# get the chat data
all_data = query_data(start_date, end_date)

# get the annotated 
annotated_df = all_data[all_data.EXPECTED_ANNOTATION==True]
# get the transferred conversation ids 
convo_id_transferred = all_data[all_data.TRANSFERRED_TO_LIVE_AGENT==True].CONVERSATION_ID.unique()
# get the non-annotated and non-transferred conversations 
no_annotate_df = all_data[(all_data.EXPECTED_ANNOTATION==False)&(~all_data.CONVERSATION_ID.isin(convo_id_transferred))]
# group by month_year, get the conversation id counts 
total = all_data.groupby('month_year')['CONVERSATION_ID'].nunique().reset_index()
total.columns = ['month_year', 'total conversation count']

annotated_total = annotated_df.groupby('month_year')['CONVERSATION_ID'].nunique().reset_index()
annotated_total.columns = ['month_year', 'annotated conversation count']

transferred = all_data[all_data.TRANSFERRED_TO_LIVE_AGENT==True].groupby('month_year')['CONVERSATION_ID'].nunique().reset_index()
transferred.columns = ['month_year', 'transferred conversation count']

no_transferred_no_annotated = no_annotate_df.groupby('month_year')['CONVERSATION_ID'].nunique().reset_index()
no_transferred_no_annotated.columns = ['month_year', 'non-transferred and non-annotated conversation count']

# merge those tables 
df1 = pd.merge(total, annotated_total, on='month_year')
df2 = pd.merge(df1, transferred, on='month_year')
df3 = pd.merge(df2, no_transferred_no_annotated, on='month_year')

# display the table with all statistics 
st.table(df3[df3['total conversation count']!=0])

# display the conversations in September, investigating the conversation level is unique for one conversation. 
annotated_df_sep = annotated_df[annotated_df.month_year == 'September-2022']
annotated_df_sep_gr = annotated_df_sep.groupby('CONVERSATION_ID').apply(lambda x: ''.join(x.CONVERSATION_LEVEL)).reset_index()
annotated_df_sep_gr.columns = ['CONVERSATION_ID', 'LEVEL']

conv_level = annotated_df_sep_gr.LEVEL.unique()
st.markdown('Note: Conversation Level is aggregated by conversation id, investigating what conversation flag are there in one conversation. \n  Ideally one conversation has one conversation level flag.')
conv_selected = st.selectbox("Converation Level:", conv_level) 
conv_id_selected = st.selectbox("Converation IDs:", annotated_df_sep_gr[annotated_df_sep_gr.LEVEL==conv_selected].CONVERSATION_ID.unique())
st.markdown(f"Total number of conversations: {len(conv_id_selected)} in the selected conversation levels")
st.dataframe(annotated_df_sep[annotated_df_sep.CONVERSATION_ID == conv_id_selected])

st.markdown("### Interesting checking out conversations that are not transferred and not annotated??") 
samples = no_annotate_df
conv_id_selected2 = st.selectbox("Conversation IDs:", samples.CONVERSATION_ID.unique())
st.dataframe(samples[samples.CONVERSATION_ID == conv_id_selected2])
