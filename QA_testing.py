import qa_checks.QualityAssurance as QA

# [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason]

data0 = [['', '', '', '', '', '', ''],
        ['', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '']]

data1 = [['', '', '', '', '', '', '']]

data2 = [['1099', '1099_general', 'Contained', '', 'Full Containment', 'known intent', '']]

data3 = [['1099', '1099_general', 'Contained', '', 'Full Containment', 'known intent', ''],
        ['1099', '1099_general', 'Contained', '', 'Full Containment', 'known intent', ''],
        ['1099', '1099_general', 'Contained', '', 'Full Containment', 'known intent', '']]

data4 = [['1099', '1099_general', 'Contained', '', 'Partial Containment', 'known intent', ''],
        ['1099', '1099_general', '', '', '', 'known intent', ''],
        ['1099', '1099_general', '', '', '', 'known intent', '']]

data5 = [['unknown', 'None', '', '', '', 'known intent', ''],
        ['unknown', 'None', 'Contained', '', 'Partial Containment', 'known intent', '']]

# result0 = QA.QualityAssurance(data0).get_qa_result() == False
# result1 = QA.QualityAssurance(data1).get_qa_result() == False
# result2 = QA.QualityAssurance(data2).get_qa_result() == True
# result3 = QA.QualityAssurance(data3).get_qa_result() == False
# result4 = QA.QualityAssurance(data4).get_qa_result() == False
result5 = QA.QualityAssurance(data5).get_qa_result() == False
# print(result0, result1, result2, result3, result4)