#### CLASS TO EXAMINE ONE UTTERANCE AT A TIME ######
#### data in format:
#        [conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason]

class UtteranceQA:
    def __init__(self, data):
        self.data = data
        self.conversation_level = data[0].lower()
        self.abandon_reason = data[1].lower()
        self.contained_reason = data[2].lower()
        self.utterance_type = data[3].lower()
        self.escalation_reason = data[4].lower()
        pass

    def check_conversation_level(self):
        if self.conversation_level == 'abandoned' or self.conversation_level == 'contained' or\
            self.conversation_level == 'transfered' or self.conversation_level == '':
            return True
        else:
            return False

    def check_abandon_reason(self):
        if self.conversation_level == 'abandoned':
            if self.abandon_reason == '' or self.abandon_reason == 'greeting' or\
                self.abandon_reason == 'intent':
                return True
        else:
            if self.abandon_reason == '':
                return True
        return False

    def check_contained_reason(self):
        if self.conversation_level == 'contained':
            if self.contained_reason == 'full containment':
                return True
        elif self.conversation_level == 'abandoned' or self.conversation_level == 'transferred':
            if self.contained_reason == '' or self.contained_reason == 'partial containment':
                return True
        else:
            if self.contained_reason == '':
                return True
        return False

    def check_utterance_type(self):
        if self.conversation_level == 'abandoned':
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or\
                self.utterance_type == 'escalation' or self.utterance_type == 'sentiment':
                return True
        elif self.conversation_level == 'contained':
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent':
                return True
        elif self.conversation_level == 'transferred':
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or\
                self.utterance_type == 'escalation' or self.utterance_type == 'sentiment' or\
                self.utterance_type == 'designed_transfer':
                return True
        else:
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or\
                self.utterance_type == 'escalation' or self.utterance_type == 'sentiment' or\
                self.utterance_type == 'designed_transfer':
                return True
        return False

    def check_escalation_reason(self):
        if self.utterance_type == 'escalation':
            if self.escalation_reason == 'greeting' or self.escalation_reason == 'intent':
                return True
        else:
            if self.escalation_reason == '':
                return True
        return False

    def update_qa_result(self):
        convo_level_qa = self.check_conversation_level()
        abandon_reason_qa = self.check_abandon_reason()
        contained_reason_qa = self.check_contained_reason()
        utterance_type_qa = self.check_utterance_type()
        escalation_reason_qa = self.check_escalation_reason()
        if convo_level_qa and abandon_reason_qa and contained_reason_qa and utterance_type_qa and escalation_reason_qa:
            return True
        return False
