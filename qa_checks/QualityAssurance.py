import qa_checks.ConvoQA as ConvoQA
import qa_checks.IntentQA as IntentQA
import qa_checks.UtteranceQA as UtteranceQA

#### CLASS TO EXAMINE ONE CONVO AT A TIME ######
#### data in format:
#    [[business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason]]
class QualityAssurance():
    def __init__(self, convo_data):
        self.convo_data = convo_data
        self.valid_utterance = None
        self.valid_intent = None
        self.valid_convo = None
        self.failed_turn = None
        self.valid = self.update_qa_result()
        pass

    def get_qa_result(self):
        return self.valid

    def get_valid_utterance(self):
        return self.valid_utterance

    def get_valid_intent(self):
        return self.valid_intent

    def get_valid_convo(self):
        return self.valid_convo

    def get_failed_turn(self):
        return self.failed_turn

    def update_qa_result(self):
        self.valid_convo = ConvoQA.ConvoQA(self.convo_data).update_qa_result()
        turn_cnt = 0
        for utterance in self.convo_data:
            self.valid_intent = IntentQA.IntentQA(utterance[0:2]).update_qa_result()
            self.valid_utterance = UtteranceQA.UtteranceQA(utterance[2:]).update_qa_result()
            if not self.valid_intent or  not self.valid_utterance:
                self.failed_turn = turn_cnt
                break
            turn_cnt += 1
        return self.valid_convo and self.valid_intent and self.valid_utterance