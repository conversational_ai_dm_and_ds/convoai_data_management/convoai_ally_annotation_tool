#### CLASS TO EXAMINE A CONVO AT A TIME ######
#### data in format:
#    [[business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason]]

class ConvoQA:
    def __init__(self, data):
        self.data = data
        pass

    def count_blanks(self, values):
        cnt = 0
        for val in values:
            if val != '':
                cnt += 1
        return cnt

    def check_convo_level(self, convo_levels):
        return self.count_blanks(convo_levels) == 1
        
    def check_aban_reason(self, aban_reasons):
        return self.count_blanks(aban_reasons) <= 1

    def check_cont_reason(self, cont_reasons):
        return self.count_blanks(cont_reasons) <= 1

    def check_esc_reason(self, esc_reasons):
        return self.count_blanks(esc_reasons) <= 1

    def update_qa_result(self):
        convo_levels = []
        aban_reasons = []
        cont_reasons = []
        esc_reasons = []
        for utterance in self.data:
            convo_levels.append(utterance[2])
            aban_reasons.append(utterance[3])
            cont_reasons.append(utterance[4])
            esc_reasons.append(utterance[6])
        convo_qa = self.check_convo_level(convo_levels)
        aban_qa = self.check_aban_reason(aban_reasons)
        cont_qa = self.check_cont_reason(cont_reasons)
        esc_qa = self.check_esc_reason(esc_reasons)
        qa_convo_level = convo_qa and aban_qa and cont_qa and esc_qa
        return qa_convo_level