#### CLASS TO EXAMINE INTENTS ONE UTTERANCE AT A TIME ######
#### data in format:
#        [business_intent, rasa_intent]

# import sys
# # sys.path.append("\snowflake_con")
# print(sys.path)
# from ..snowflake_con import db_connection
import snowflake_con.db_connection as db_connection

class IntentQA:
    def __init__(self, data):
        self.data = data
        self.business_intent = data[0].lower()
        self.rasa_intent = data[1].lower()

    def get_business_intents(self): 
        sc = db_connection.snowflake_connector()
        query = f'''select distinct intent_business_groups as INTENT_BUSINESS_GROUPS, 
            intent as RASA_INTENTS 
            FROM TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_BUSINESS_INTENT_NAMES;'''
        sc.create_cursor()
        df = sc.query_data(query)
        sc.cursor.close()
        return df

    def update_qa_result(self):
        df = self.get_business_intents()
        return ((df['INTENT_BUSINESS_GROUPS'].str.lower() == self.business_intent) &\
             (df['RASA_INTENTS'].str.lower() == self.rasa_intent)).any()
